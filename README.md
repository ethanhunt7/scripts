## Arch Linux Installation Script

These are basic Arch Linux install and post-install scripts to install Arch Linux easily on a UEFI system. You can configure them to your liking.

Full credit for these scripts goes to krushndayshmookh whose scripts are available on this link:
https://github.com/krushndayshmookh/krushn-arch

Given below are the parameters that you should/can change in the scripts:

**Please make sure to select the right disk before partitions are made by the script by running _# lsblk_. Please make sure that sda is the drive that you want to format.**

### Install.sh script:
1. **Drive letters (Lines 48-50):**
You can do this by running the lsblk command and confirming the drive letter. Please note that this is a very important step or else this can lead to data loss of the wrongly formatted drive.

2. **Partition sizes (Lines 60-78):**
You can change the partition sizes or number of partitions in the script from lines 60-78. For example, if you want to make a swap file later, and not make a swap partition, you can eliminate the sda2 partition altogether in the script.

3. **Formatting the partition (Lines 88-89):**
If you have changed the drive above, please make the same changes here (sda to sdb or sdc, etc.)

4. **Mounting the partitions (Lines 95-97):**
If you have made changes in the drive letters and removed the swap partition, please make changes here accordingly.

### Post-install script
1. **Swappiness value (Line 24):** 
You can change the swappiness value if you want. This value is 60 by default. 10 is recommended.

2. **Time Zone (Line 33):**
It is set to Asia/Kolkata by default. You can change it by following this article https://archlinuxblog.medium.com/installing-arch-linux-e11f3e302adf

3. **Locale (Lines 44 and 46):** 
It is set to en_US.UTF-8 UTF-8 by default.

4. **Hostname (Line 55 and 58):** 
It is set to arch by default.

5. **Boot mount drive (Line 89):** 
It is set to sda by default. Please make changes here as per the changes made in the Install script.

6. **Username (Line 105 and 114):** 
It is set to hunt by default. Please change this to the username you want.

7. **Base applications (Line 151):** 
You can remove or add base applications if needed.

8. **Ucode (Lines 161 and 164):** 
Comment out as per your processor. If installing on a virtual machine, please do not comment out.

9. **VirtualBox installation (Lines 173-176):** 
If you want to install VirtualBox on your new Arch system, please comment out the lines.

10. **Laptop packages (Line 185):** 
You can uncomment this line if you are installing the system on a laptop.

11. **Printer packages (Line 194):** 
You can uncomment this line if you want to use a printer with the system.

12. **Enabling services (Lines 203-206 and 209-210):** 
Please enable the services according to the packages that you have installed above.

13. **Enabling TRIM (Line 219):** 
Please enable this line if you are running the system on an SSD drive.

### Instructions:

1. Run the following commands after you boot from the ISO:
```bash
pacman -Sy
pacman -S git wget vim
```

2. Clone the repository
```bash
git clone https://gitlab.com/ethanhunt7/scripts
```

At this step, please use vim or nano to edit both the install.sh and the post-install.sh files and change the above mentioned parameters like drive letter, packages, user name, etc.

3. Run these commands:
```bash
cd scripts/arch/
chmod +x install.sh
./install.sh
```
4. After the base installation is done, and the post-install.sh file has been copied to the root directory, run these commands:
```bash
cd
./post-install.sh
```

And you are done!

