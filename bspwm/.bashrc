#
#  ______ _    _
# |  ____| |  | |
# | |__  | |__| |
# |  __| |  __  |
# | |____| |  | |
# |______|_|  |_|
#
#
# ~/.bashrc
#

### Archive Extraction
## ex = Extractor for all kinds of archives
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Settings

# TERMCAP Setup
# enter blinking mode - red
export LESS_TERMCAP_mb=$(printf '\e[01;31m')
# enter double-bright mode - bold, magenta
export LESS_TERMCAP_md=$(printf '\e[01;35m')
# turn off all appearance modes (mb, md, so, us)
export LESS_TERMCAP_me=$(printf '\e[0m')
# leave standout mode
export LESS_TERMCAP_se=$(printf '\e[0m')
# enter standout mode - green
export LESS_TERMCAP_so=$(printf '\e[01;32m')
# leave underline mode
export LESS_TERMCAP_ue=$(printf '\e[0m')
# enter underline mode - blue
export LESS_TERMCAP_us=$(printf '\e[04;34m')

# Add custom enviroment
PATH=$PATH:~/Scripts

# PS1 Setup
PROMPT_COMMAND=__prompt_command

__prompt_command() {
    local EXITCODE="$?"

    local HOSTCOLOR="5"
    local USERCOLOR="3"
    local PATHCOLOR="4"

    PS1="\e[3${HOSTCOLOR}m \h  \e[3${USERCOLOR}m \u  \e[3${PATHCOLOR}m \w  \n";

    if [ $EXITCODE == 0 ]; then
        PS1+="\e[32m\$ \e[0m";
    else
        PS1+="\e[31m\$ \e[0m";
    fi
}


export HISTCONTROL=ignoreboth:erasedups

#PS1='[\u@\h \W]\$ '

#if [ -d "$HOME/.bin" ] ;
#  then PATH="$HOME/.bin:$PATH"
#fi

#if [ -d "$HOME/.local/bin" ] ;
#  then PATH="$HOME/.local/bin:$PATH"
#fi


### Aliases

## Shell Commands
# Changing "ls" to "exa"
alias ls="exa -al --color=always --group-directories-first"		# my preferred listing
alias els="els --els-icons=fontawesome"					# els listing
alias la="exa -a --color=always --group-directories-first"  		# all files and dirs
alias ela="els -laH --els-icons=fontawesome"				# ela files and dirs
alias ll="exa -l --color=always --group-directories-first"  		# long format
alias lt="exa -aT --color=always --group-directories-first" 		# tree listing
alias l.="exa -a | egrep "^\.""
alias lsblkl="lsblk -o name,size,type,mountpoint,label,model,uuid"
alias lsblkf="lsblk -o name,fstype,size,fsavail,fsuse%,label,mountpoint"

# Colorize grep output (good for log files)
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# Get top process eating memory
alias psmem="ps auxf | sort -nr -k 4"
alias psmem10="ps auxf | sort -nr -k 4 | head -10"

# Get top process eating cpu
alias pscpu="ps auxf | sort -nr -k 3"
alias pscpu10="ps auxf | sort -nr -k 3 | head -10"

# Confirm before overwriting something
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

# Adding flags
alias df="df -kTh"		# human-readable sizes
alias free="free -h"		# show sizes in MB
alias du="du -kh"
alias mkdir="mkdir --parents"
alias less="less -r"

# Backup
alias backupconf="sudo cp -Rf ~/.config /mnt/fbbe47d8-f647-4e7c-a299-95f9767405e5/Backup/.config-backup-$(date +%Y.%m.%d-%H.%M.%S)"		# backup ~/.config folder
alias backuphome="sudo cp -Rf ~/ /mnt/fbbe47d8-f647-4e7c-a299-95f9767405e5/Backup/home-backup-$(date +%Y.%m.%d-%H.%M.%S)"			# backup ~/ folder

# Navigation
alias ..="cd .."
alias ...="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."
alias .5="cd ../../../../.."

# Fix obvious typos
alias cd..="cd .."
alias pdw="pwd"
alias udpate="sudo pacman -Syyu"
alias upate="sudo pacman -Syyu"

# Grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# Switch between bash and zsh
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"

# Hardware info --short
alias hw="hwinfo --short"

# Check vulnerabilities microcode
alias microcode="grep . /sys/devices/system/cpu/vulnerabilities/*"

# Get fastest mirrors in your neighborhood
alias mirror="sudo reflector --latest 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist"

# Get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"


## Software Management
# Protonvpn commands
alias statusp="protonvpn status"
alias connect="sudo protonvpn c -f"
alias reconnect="sudo protonvpn r"
alias random="sudo protonvpn c -r"
alias connectus="sudo protonvpn c --cc US"
alias disconnect="sudo protonvpn d"

# Redshift
alias redshift="redshift -l 19.07283:72.88261"

# Pihello
alias pihole="pihello 10.10.1.2"

# Alsa mixer volume control
alias vol="alsamixer"

# Ranger
alias ran="ranger"
alias sran="sudo ranger"

# Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

# Git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'
alias tag='git tag'
alias newtag='git tag -a'


## Shell
# Shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"

# Load configs
alias bashrc="sudo vim ~/.bashrc"
alias sxhkd="vim ~/.config/sxhkd/sxhkdrc"
alias bspwm="vim ~/.config/bspwm/bspwmrc"
# alias polybar="vim ~/.config/polybar/config"

# Reload bashrc
alias reload="source ~/.bashrc"


## Pacman
# Pacman and yay
alias pacsyu="sudo pacman -Syyu"							# update only standard pkgs
alias piksua="pikaur -Sua --noconfirm"							# update only AUR pkgs
alias piksyu="pikaur -Syu --noconfirm"							# update standard pkgs and AUR pkgs
alias unlock="sudo rm /var/lib/pacman/db.lck"						# remove pacman lock
alias cleanup="sudo pacman -Rns $(pacman -Qtdq)"					# remove orphaned packages
alias upgrade="sudo pacman -Syy && yay -Syua --noconfirm"
alias update="sudo pacman -Syyu"
alias sync="sudo pacman -Syy"

# Pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"


## GPG
# Verify signature for ISOs
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"

# Receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"


### Fun Stuff
# Starwars
alias starwars="telnet towel.blinkenlights.nl"

# The Fuck
eval $(thefuck --alias)
# You can use whatever you want as an alias, like for Mondays:
eval $(thefuck --alias FUCK)

# Fortune Cookies... with a cow
fortune -a | cowthink -f $(find /usr/share/cows -type f | shuf -n 1)

# The terminal Rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

