#! /bin/bash
#
#
#  ______ _    _
# |  ____| |  | |
# | |__  | |__| |
# |  __| |  __  |
# | |____| |  | |
# |______|_|  |_|
#
#
# This is Ethan Hunt's BSPWM Installation Script.
#

###########  Set display drivers  #################################
###################################################################

echo -e '\n\n## Installing display drivers\n\n' 

# Intel 
# sudo pacman -S --noconfirm xf86-video-intel

# AMD
# sudo pacman -S --noconfirm xf86-video-amdgpu

# NVIDIA
# sudo pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

# ATI
# sudo pacman -S --noconfirm xf86-video-ati

# Virtualbox
sudo pacman -S --noconfirm xf86-video-vmware

###########  Install an AUR helper  ###############################
###################################################################

echo -e '\n\n## Installing an AUR helper\n\n' 

git clone https://aur.archlinux.org/pikaur.git
cd pikaur/
makepkg -si --noconfirm

###########  Install & enable UFW  ################################
###################################################################

# echo -e '\n\n## Installing & enabling UFW\n\n' 

# sudo pacman -S --noconfirm ufw
# sudo systemctl enable ufw.service --now
# sudo ufw enable
# sudo ufw allow SSH
# sudo ufw limit SSH

###########  Install BSPWM, applications & fonts  #################
###################################################################

echo -e '\n\n## Installing BSPWM, applications & fonts\n\n' 

sudo pacman -S --noconfirm arc-gtk-theme arc-icon-theme alacritty bspwm cmatrix cmus cowsay dmenu dunst exa file-roller firefox fprintd fortune-mod geany galculator gnome-disk-utility kitty lxappearance lynx mousepad nemo neofetch nitrogen noto-fonts-emoji numlockx pcmanfm picom playerctl redshift sl speedtest-cli sxhkd thefuck tldr trash-cli ttf-dejavu ttf-droid ttf-font-awesome ttf-hack ttf-inconsolata ttf-jetbrains-mono ttf-liberation ttf-roboto vlc vnstat volumeicon xfce4-power-manager xfce4-settings xfce4-taskmanager xorg xorg-drivers xorg-xinit

###########  Install Archcraft applications  ############################
###################################################################

# echo -e '\n\n## Installing Archcraft applications\n\n' 

# sudo pacman -S --noconfirm xorg-fonts-misc xorg-xfd  xorg-xinput xf86-input-libinput xf86-video-fbdev xf86-video-vesa networkmanager-openvpn feh imagemagick viewnior ffmpeg ffmpegthumbnailer gst-libav gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly mpc mpd mplayer ncmpcpp tumbler jq gparted sshfs gtk-engine-murrine pv xsettingsd yad xclip kvantum-qt5 qt5ct 

###########  Install AUR applications  ############################
###################################################################

# echo -e '\n\n## Installing AUR applications\n\n' 

# pikaur -S --noconfirm nerd-fonts-mononoki pfetch cava
# pikaur -S --noconfirm polybar

###########  Copy config files  ###################################
###################################################################

echo -e '\n\n## Copying config files\n\n' 

# Make directories
sudo mkdir -p ~/.config/{bspwm,sxhkd}

# Copy config files to the directories
# sudo cp -rfv ~/scripts/bspwm/bspwmrc ~/.config/bspwm
# sudo cp -rfv ~/scripts/bspwm/sxhkdrc ~/.config/sxhkd
# sudo cp -rfv ~/scripts/bspwm/.xinitrc ~/
# sudo cp -rfv ~/scripts/bspwm/.bashrc ~/

# echo "MAIN PACKAGES"

# sleep 5

# sudo pacman -S --noconfirm xorg arandr

# sudo systemctl enable lightdm

# mkdir -p .config/{bspwm,sxhkd,dunst}

# install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
# install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

# printf "\e[1;32mCHANGE NECESSARY FILES BEFORE REBOOT\e[0m"

