#! /bin/bash
#
#
#  ______ _    _
# |  ____| |  | |
# | |__  | |__| |
# |  __| |  __  |
# | |____| |  | |
# |______|_|  |_|
#
#
# This is Ethan Hunt's Arch Linux Post Install Script.
#

echo "Ethan Hunt's Arch Configurator"

###########  Set swappiness value  ################################
###################################################################

echo -e '\n\n## Setting swappiness value to 10\n\n'

sleep 5

echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf

###########  Set date and time  ###################################
###################################################################

echo -e '\n\n## Setting time zone and clock\n\n'

sleep 5

ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
hwclock --systohc

###########  Set locale  ##########################################
###################################################################

echo -e '\n\n## Setting up locale\n\n'

sleep 5

# Set locale to en_US.UTF-8 UTF-8
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "LC_CTYPE=en_US.UTF-8" >> /etc/locale.conf

###########  Set hostname  ########################################
###################################################################

echo -e '\n\n## Set hostname\n\n'

sleep 5

echo "arch" >> /etc/hostname
echo "127.0.0.1     localhost" >> /etc/hosts
echo "::1           localhost" >> /etc/hosts
echo "127.0.1.1     arch.localdomain    arch" >> /etc/hosts

###########  Generate initramfs  ##################################
###################################################################

echo -e 'Generating initramfs\n\n'

sleep 5

mkinitcpio -p linux

###########  Set root password  ###################################
###################################################################

echo -e '\n\n## Setting root password\n\n'

passwd

###########  Install GRUB  ########################################
###################################################################

echo -e '\n\n## Installing bootloader\n\n'

sleep 5

pacman -S --noconfirm grub os-prober efibootmgr

sleep 5

# Mount the boot partition
mkdir -p /boot/efi
mount /dev/sda1 /boot/efi

sleep 5

# Install bootloader
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

###########  Create a new user  ###################################
###################################################################

echo -e '\n\n## Creating a new user\n\n'

sleep 5

# Add a new user
useradd -m -g users -G wheel,power,input,storage,network hunt

sleep 5

# Place the new user under the wheel group
sed --in-place 's/^#\s*\(%wheel\s\+ALL=(ALL:ALL)\s\+ALL\)/\1/' /etc/sudoers

echo -e '\n\n## Set password for the new user\n\n'

passwd hunt

###########  Set pacman options  ##################################
###################################################################

echo -e '\n\n## Setting pacman options\n\n'

sleep 5

# Initate pacman keyring
# pacman-key --init
# pacman-key --populate archlinux

sleep 5

# Enable color output
sed -i '/Color/s/^#//' /etc/pacman.conf

# Enable paralell downloads
sed -i '/ParallelDownloads = 5/s/^#//' /etc/pacman.conf

# Enable pacman
sed -i '/Color/a ILoveCandy' /etc/pacman.conf

# Enable multilib repository
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf

sleep 5

# Create the fastest mirrorlist with https
reflector --latest 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Sy

###########  Install base applications  ###########################
###################################################################

echo -e '\n\n## Installing base applications\n\n'

sleep 5

pacman -S --noconfirm alsa-plugins alsa-tools alsa-utils bash-completion bind bluez bluez-utils dialog dhcpcd dnsmasq dosfstools exfat-utils git gvfs gvfs-afc gvfs-smb gvfs-mtp htop linux-headers lsb-release man-db man-pages mesa mlocate mtools networkmanager network-manager-applet ntfs-3g openssh p7zip pacman-contrib pavucontrol pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer-ladspa smartmontools terminus-font usbmuxd usbutils unrar unzip wget wireless_tools wpa_supplicant xdg-user-dirs xdg-utils reflector vim xarchiver zip

###########  Install ucode  #######################################
###################################################################

echo -e '\n\n## Installing ucode\n\n'

sleep 5

# Intel
pacman -S --noconfirm intel-ucode

# AMD
# pacman -S --noconfirm amd-ucode

echo -e '\n\n## Regenerate GRUB\n\n'

grub-mkconfig -o /boot/grub/grub.cfg

###########  Install Vitrualbox  ##################################
###################################################################

# echo -e '\n\n## Installing virtualbox\n\n'

# sleep 5

# pacman -S --noconfirm virtualbox virtualbox-host-modules-arch
# modprobe vboxdrv
# usermod -aG vboxusers hunt
# pacman -S --noconfirm virtualbox-guest-iso

###########  Install laptop packages  #############################
###################################################################

# echo -e '\n\n## Installing laptop packages\n\n'

# sleep 5

# pacman -S --noconfirm acpi acpi_call acpid tlp

###########  Install printer packages  ############################
###################################################################

# echo -e '\n\n## Installing printer packages\n\n'

# sleep 5

# pacman -S --noconfirm cups hplip gutenprint

###########  Enable services  #####################################
###################################################################

echo -e '\n\n## Enabling services\n\n'

sleep 5

systemctl enable NetworkManager
systemctl enable dhcpcd
systemctl enable bluetooth
systemctl enable sshd

# Laptop services
# systemctl enable tlp
# systemctl enable acpid

###########  Enable SSD TRIM  #####################################
###################################################################

echo -e '\n\n## Enabling SSD TRIM\n\n'

sleep 5

systemctl enable fstrim.timer

echo "Configuration done. You can now exit chroot."
