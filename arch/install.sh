#! /bin/bash
#
#
#  ______ _    _
# |  ____| |  | |
# | |__  | |__| |
# |  __| |  __  |
# | |____| |  | |
# |______|_|  |_|
#
#
# This is Ethan Hunt's Arch Linux Install Script.
#

echo -e "\n\n ______ _    _"
echo "|  ____| |  | |"
echo "| |__  | |__| |"
echo "|  __| |  __  |"
echo "| |____| |  | |"
echo "|______|_|  |_|"

echo -e "\nEthan Hunt's Arch Installer\n\n"

###########  Check network connection  ############################
###################################################################

echo "##### Checking network connection #####"
echo "#######################################"

# echo -e '\n\n## Checking network connection\n\n'

wget -q --spider https://google.com

if [ $? -eq 0 ]; then
    echo -e "\n# You are connected\n\n\n\n"
else
    echo "Connect to the internet to continue..."
    exit 1
fi

sleep 5

###########  Set up time  #########################################
###################################################################

# echo -e "\n#########################"
# echo "## Setting system time ##"
# echo "#########################"

echo "######### Setting system time #########"
echo "#######################################"

# echo -e '\n\n## Setting system time'

timedatectl set-ntp true

echo -e '\n# Time is set\n\n\n\n'

sleep 5

###########  Set pacman options  ##################################
###################################################################

# echo "############################"
# echo "## Setting pacman options ##"
# echo "############################"

echo "######## Setting pacman options #######"
echo "#######################################"

# echo -e '## Setting pacman options\n\n'

# Enable color output
sed -i '/Color/s/^#//' /etc/pacman.conf

echo -e '\n\n# Color is set\n\n'

sleep 5

# Enable pacman
sed -i '/Color/a ILoveCandy' /etc/pacman.conf

echo -e '# ILoveCandy is enabled\n'

sleep 5

# Enable paralell downloads
sed -i '/ParallelDownloads = 5/s/^#//' /etc/pacman.conf

echo -e '\n# Parallel downloads have been enabled\n\n'

sleep 5

# Create the fastest mirrorlist with https
echo -e '# Setting fastest mirrors\n\n'

reflector --latest 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

echo -e '\n\n# Fastest mirrors are set\n'

sleep 5

pacman -Sy

echo -e '\n\n# Pacman is synced\n\n'

sleep 5

###########  Create, format & mount partitions  ###################
###################################################################

echo "## Create, format & mount partitions ##"
echo "#######################################"

# echo "##################################################"
# echo "## Creating, formatting and mounting partitions ##"
# echo "##################################################"

# echo -e '\n\nCreating, formatting and mounting partitions \n\n'

sleep 3

echo -e '\n# Wiping the partition table\n'

dd if=/dev/zero of=/dev/sda bs=1M count=1
 
sleep 3

echo "This script will create and format the partitions as follows:"
echo "/dev/sda1 - 500Mib will be mounted as /boot/efi"
echo "/dev/sda2 - 24GiB will be used as swap"
echo "/dev/sda3 - rest of space will be mounted as /"
read -p 'Continue? [y/N]: ' fsok
if ! [ $fsok = 'y' ] && ! [ $fsok = 'Y' ]
then 
    echo "Edit the script to continue..."
    exit
fi

# to create the partitions programatically (rather than manually)
# https://superuser.com/a/984637
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # default - start at beginning of disk 
  +500M # 500 MB boot parttion
  n # new partition
  p # primary partition
  2 # partion number 2
    # default, start immediately after preceding partition
  +24G # 24 GB swap parttion
  n # new partition
  p # primary partition
  3 # partion number 3
    # default, start immediately after preceding partition
    # default, extend partition to end of disk
  a # make a partition bootable
  1 # bootable partition is partition 1 -- /dev/sda1
  p # print the in-memory partition table
  w # write the partition table
  q # and we're done
EOF

echo -e '\n# Partitions created\n'

sleep 5

echo -e '\n# Formatting the partitions\n\n'

mkfs.ext4 /dev/sda3
mkfs.fat -F32 /dev/sda1

echo -e '# Partitions formatted\n'

sleep 5

echo -e '# Mounting the partitions\n\n'

sleep 5

mount /dev/sda3 /mnt

echo -e '# Root partition mounted\n'

sleep 5

echo -e '\n# Making Swap and turning it on\n\n'

mkswap /dev/sda2
swapon /dev/sda2

echo -e '\n\n# Swap is now on\n'

sleep 5

###########  Install the base system  #############################
###################################################################

echo -e '\n\n# Installing the base system\n\n'

pacstrap /mnt base base-devel linux linux-firmware

echo -e '\n# Base system is now installed\n'

sleep 5

###########  Generate fstab  ######################################
###################################################################

sleep 5

echo -e '\n\n# Generating fstab\n\n'

genfstab -U /mnt >> /mnt/etc/fstab

echo -e '\n# fstab is generated\n'

sleep 5

###########  Copy post-install script  ############################
###################################################################

sleep 5

echo -e '\n\n# Copying post-install script to new /root\n\n'

sleep 5

cp -rfv post-install.sh /mnt/root
chmod a+x /mnt/root/post-install.sh

###########  Chroot & finish  #####################################
###################################################################

echo -e '\n\n# Chroot into new system\n\n'

echo "After chrooting into newly installed OS, please run the post-install.sh by executing ./post-install.sh"
echo "Press Enter to chroot..."
read tmpvar
arch-chroot /mnt /bin/bash

echo -e 'Finishing\n\n'
echo "If post-install.sh was run succesfully, you will now have a fully working bootable Arch Linux system installed."
echo "The only thing left is to reboot into the new system."
echo "Press any key to reboot..."
read tmpvar
reboot

